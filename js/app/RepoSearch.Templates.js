(function() {
  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['appLayout'] = template(function (Handlebars,depth0,helpers,partials,data) {
  this.compilerInfo = [4,'>= 1.0.0'];
helpers = this.merge(helpers, Handlebars.helpers); data = data || {};



  return "<header id=\"reposearch-header\" class=\"header header-logged-in true\"></header>\n<section id=\"reposearch-main\"></section>\n<footer id=\"reposearch-footer\"></footer>";
  });
})();