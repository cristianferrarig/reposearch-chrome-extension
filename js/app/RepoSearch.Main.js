'use strict';

RepoSearch.module('Main', function (Main, App, Backbone, Marionette, $) {
  Main.Controller = function(){}

  _.extend(Main.Controller.prototype, {
    start: function(){
      this.showMainLayout();
    },

    showMainLayout: function(){
      if(App.$repoSearchContainer !== undefined){
        var mainLayout = new RepoSearch.Views.AppLayout();
        App.$repoSearchContainer.html(mainLayout.render().el);
      }
    }
  })

  Main.addInitializer(function(){
    var mainController = new Main.Controller();
    mainController.start();
  });

});