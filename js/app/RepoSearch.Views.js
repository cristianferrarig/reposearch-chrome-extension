'use strict';

RepoSearch.module('Views', function (Views, App, Backbone, Marionette, $) {

  Views.AppLayout = Marionette.Layout.extend({

    tagName: "section",
    id: "reposearch-app",

    template: function(){
      return Handlebars.templates.appLayout();
    },

    regions: {
      header: "#reposearch-header",
      main: "#reposearch-main",
      footer: "#reposearch-footer"
    }

  });

});