// Add manifest access to the extension
chrome.manifest = chrome.app.getDetails();

if($('#repo-search').length === 0){

  RepoSearch.on("initialize:before", function(options){
    console.log("initialize:before");
    RepoSearch.$repoSearchContainer = $("<div id='reposearch-container'></div>");
    $('.wrapper').after(RepoSearch.$repoSearchContainer);
  });

  RepoSearch.start();
}


